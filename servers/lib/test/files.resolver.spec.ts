import { Test, TestingModule } from "@nestjs/testing";
import { FilesService } from "../src/files/files.service";
import { FilesResolver } from "../src/files/files.resolver";

describe("FilesResolver", () => {
  let filesResolver: FilesResolver;

  const mockFilesService = {
    getFilesInDirectory: jest.fn(() => ["1"]),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FilesResolver, FilesService],
    })
      .overrideProvider(FilesService)
      .useValue(mockFilesService)
      .compile();

    filesResolver = module.get<FilesResolver>(FilesResolver);
  });

  it("should be defined", () => {
    expect(filesResolver).toBeDefined();
  });

  describe("getFiles", () => {
    it("should be defined", () => {
      expect(filesResolver.getFiles).toBeDefined();
    });

    it("should return the filenames in the given directory", () => {
      // the mockvalue that our test will use
      const files = ["1"];

      // compare the result to the expected value
      expect(filesResolver.getFiles("")).toStrictEqual(files);
    });
  });
});

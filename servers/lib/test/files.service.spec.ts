import { Test, TestingModule } from "@nestjs/testing";
import { FilesService } from "../src/files/files.service";
import { ConfigService } from "@nestjs/config";

describe("FilesService", () => {
  let filesService: FilesService;

  // Mocked value of our ConfigService, so it just gives a hardcoded value out,
  // so that we dont increase the complexity of our test
  const mockConfigService = {
    get: jest.fn(() => "/Users/phillipravn/DTaaS/data/assets/user"),
  };

  // create a module with the filesService and the ConfigService, and override the ConfigService with our mock
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FilesService, ConfigService],
    })
      .overrideProvider(ConfigService)
      .useValue(mockConfigService)
      .compile();

    // get the filesService from the module
    filesService = module.get<FilesService>(FilesService);
  });

  // test if the filesService is defined
  it("should be defined", () => {
    expect(filesService).toBeDefined();
  });

  describe("getFilesInDirectory", () => {
    // test if the getFilesInDirectory function is defined
    it("should be defined", () => {
      expect(filesService.getFilesInDirectory).toBeDefined();
    });

    it("should return the filenames in the given directory", () => {
      // the mockvalue that our test will use (below)
      const files = ["1"];

      // mock the fs.readdirSync function to return the desired path for the test
      // we create a mock of the whole fs.readdirsync function, but we need to mock the module fs, and then the function readdirSync
      const fs = require("fs");
      jest.mock("fs", () => ({
        readdirSync: jest.fn(() => files),
      }));

      // fs.readdirSync('') is to output ["1"], since this is the hardcoded output of the mock
      // filesService.getFilesInDirectory('') is to output ["1"], since this is the actual output of the function given that path

      // compare the result to the expected value
      expect(filesService.getFilesInDirectory("")).toEqual(fs.readdirSync(""));
    });
  });
});

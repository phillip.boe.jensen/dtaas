import { Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { FilesResolver } from "./files.resolver";
import { FilesService } from "./files.service";

// istanbul ignore next
@Module({
  providers: [FilesResolver, FilesService, ConfigService],
})
export class FilesModule {}
